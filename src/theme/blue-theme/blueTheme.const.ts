export const blue = {

  '--primary' : '#534bae',
  '--primary-variant': '#5c6bc0',

  '--secondary': '#000000',
  '--secondary-variant': '#fff350',

  '--background': '#e64a19',
  '--surface': '#26a69a',
  '--dialog': '#f2f4f5',
  '--cancel': '#9E9E9E',

  '--on-primary': '#FFFFFF',
  '--on-secondary': '#ffff00',
  '--on-background': '#000000',
  '--on-surface': '#000000',
  '--on-cancel': '#000000',

  '--green': '#4caf50',
  '--red': '#f44336',
  '--yellow': '#FFD54F',
  '--blue': '#3f51b5',
  '--purple': '#9c27b0',
  '--light-green': '#80ba24',
  '--grey': '#BDBDBD',
  '--grey-light': '#EEEEEE',
  '--black': '#212121'
};

export const blue_meta = {

  'translation': {
    'name': {
      'en': 'ENGLISH_NAME',
      'de': 'GERMAN_NAME'
    },
    'description': {
      'en': 'ENGLISH_DESCRIPTION',
      'de': 'GERMAN_DESCRIPTION'
    }
  },
  'isDark': false,
  'order': 4,
  'scale_desktop': 1,
  'scale_mobile': 1,
  'previewColor': 'background'

};
